<?php

namespace App\Repositories\Backend\Merchant;

use App\Models\Merchant\Filters\MerchantFilter;
use App\Models\Merchant\Merchant;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MerchantEloquentRepository implements MerchantRepositoryContract
{
    /**
     * @var Merchant
     */
    private $merchant;

    /**
     * MerchantEloquentRepository constructor.
     * @param Merchant $merchant
     */
    public function __construct(Merchant $merchant)
    {
        $this->merchant = $merchant;
    }

    public function all($search)
    {
        return $this->merchant->where('name', 'like', '%' . $search . '%')->orderBy('name')->get();
    }

    public function paginate($data = [], $perPage = 30, $columns = ['*'])
    {
        return $this->merchant
            ->select($columns)
            ->with(['merchant_cashback', 'merchant_workday', 'city', 'bank_cashback', 'merchant_commission', 'bank'])
            ->filterBy(new MerchantFilter($data))
        //->where('parent_id','!=','00000000-0000-0000-0000-000000000000')
            ->orderBy('created_at', 'desc')
            ->paginate($perPage);
    }

    public function allWithoutRelations($columns = ['*'])
    {
        $category = $this->merchant->orderBy('created_at', 'desc')->get($columns);
        return $category;
    }

    public function findById($id)
    {
        return $this->merchant->
            with('city', 'account')->
            with('merchant_workday')->
            with('merchant_cashback')->
            with('transit_account')->
            where('id', $id)->first();
    }

    public function update(array $data, $id)
    {
        $merchant = $this->merchant->findOrFail($id);
        $merchant->setOldAttributes($merchant->getAttributes());
        DB::transaction(function () use (&$merchant, $data) {
            $merchant->update($data);
            $merchant->categories()->sync($data['merchant_category_ids']);
        });

        return $merchant;
    }

    public function destroy($id)
    {
        $merchant = $this->merchant->findOrFail($id);
        $merchant->is_active = 0;
        $merchant->save();
        return $merchant;
    }

    public function create(array $data)
    {
        $data['login']=$this->generateMd5();
        $merchant = new Merchant($data);
        $merchant->save();
        //dd($data['merchant_category_ids']);
        DB::transaction(function () use ($data, &$merchant) {
            $merchant->categories()->sync($data['merchant_category_ids']);
        });

        return $merchant;
    }

    public function allWithoutParent($search)
    {
        return $this->merchant
            ->where('name', 'like', '%' . $search . '%')
            ->where('parent_id', '!=', '00000000-0000-0000-0000-000000000000')
            ->orderBy('name')
            ->get();
    }

    public function allParent($search)
    {
        return $this->merchant
            ->where('name', 'like', '%' . $search . '%')
            ->where('parent_id', '=', '00000000-0000-0000-0000-000000000000')
            ->orderBy('name')
            ->get();
    }

    public function getAllWhereAccountBalanceGreaterThanZero()
    {
        return $this->merchant
            ->with('account')
            ->where('is_active', true)
            ->withoutGlobalScopes()
            ->get();
    }

    public function findByTransitAccountIdWithoutGlobal($id)
    {
        return $this->merchant
            ->where('transit_account_id', $id)
            ->with('transit_account')
            ->withoutGlobalScopes()
            ->first();
    }

    public function findMerchantByIdAndLockForUpdate($id)
    {
        return $this->merchant->lockForUpdate()->findOrFail($id);
    }
    
    public function GetAllMerchantByCashbackId($cashback_item_id)
    {
        $merchants = $this->merchant
            ->where('merchant_cashback_id', $cashback_item_id)
            ->orWhere('bank_cashback_id', $cashback_item_id)
            ->get();
        return $merchants;
    }

    public function updateHighestCashbackValue($id, $value)
    {
        $merchant = $this->merchant->findOrFail($id);
        $merchant->highest_cashback_value = $value;
        $merchant->save();
        return $merchant;
    }

    public function deleteImageLogo($id)
    {
        $merchant = $this->findById($id);
        $merchant->img_logo = null;
        $merchant->save();
        return $merchant;
    }

    public function deleteImageAd($id)
    {
        $merchant = $this->findById($id);
        $merchant->img_ad = null;
        $merchant->save();
        return $merchant;
    }

    public function deleteImageDetail($id)
    {
        $merchant = $this->findById($id);
        $merchant->img_detail = null;
        $merchant->save();
        return $merchant;
    }

    public function listByUserBranch()
    {
        return $this->merchant
            ->userBranch()
            ->orderBy('name')
            ->get()
            ->pluck('name', 'id')
            ->prepend("","");
    }

    public function generateLogin($id)
    {
        $merchant=$this->merchant->findOrFail($id);
        if (empty($merchant->login)){
            $merchant->setOldAttributes($merchant->getAttributes());
            $merchant->login=$this->generateMd5();
            $merchant->save();
            return $merchant;
        }
        return null;
    }

    function generateMd5(){
        return md5(microtime());
    }
}
